/* Асинхронність - це механізм, який дає можливість виконувати певні процеси,
поза сновним кодом, не перериваючи його потоку на очікування виконання 
асинхронної функції чи при виникненні в ній помилки. 
*/


function fetchUserIP(){
  return fetch("https://api.ipify.org/?format=json")
  .then((data) => data.json())
  .then((userIP) => userIP.ip)
}

function fetchUserData(ip){
return fetch(`http://ip-api.com/json/${ip}`)
  .then((data) => data.json())
  .then((userData) => userData)

}
class UserInfo{
  constructor(data){
    const { timezone, country, region, city, regionName } = data;
this.timezone = timezone;
this.country = country;
this.region = region;
this.city = city;
this.regionName = regionName;
this.createList()
  }
   createList() {
    const body = document.querySelector("body");
    const paragraph = document.querySelector("p");
   
if( paragraph !== null){
paragraph.remove()
}
const text = document.createElement('p');
text.innerText = `${this.timezone}, ${this.country}, ${this.region}, ${this.city}, ${this.regionName}`
body.appendChild(text);
}

}

async function findIP(){
  try {
    const userIP = await fetchUserIP();
    const userData = await fetchUserData(userIP);
     new UserInfo(userData);

   return userData;
  } catch (error) {
    console.error("Error:", error);
}}

const buttonFindIP = document.querySelector(".findAboveIP")

buttonFindIP.addEventListener("click", async function(){
 const info = await findIP()  
})

